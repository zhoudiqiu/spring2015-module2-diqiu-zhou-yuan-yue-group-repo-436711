<!DOCTYPE html>
<html>
    <head>
        <title>Simple file sharing site</title>
    </head>
    <body>
        <p>Your files start here:</p>
        <?php
            session_start();
            /*directory need to be modified*/
                $dir = sprintf('/srv/uploads/%s',$_SESSION['username']);
                opendir($dir);
                $file1 = scandir($dir);
                echo '<br>';
                for($i=2; $i<count($file1);$i++){
                    echo "$file1[$i]";
                    echo "<br>";
                }
            closedir($dir);   
        ?>
        <form action="read.php" method="POST">
            <input type="text" name = "file_name" placeholder="Please enter the file name that U want to open"/>
            <input type="submit" name="read"/>
        </form>
        
        <form action="destroy.php" method="POST">
        <p>You can log out here</p>
            <input type="submit" name="log out"/>
        </form>



 <form enctype="multipart/form-data" action="upload.php" method="POST">
        <p>
            <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
            <label for="uploadfile_input">Choose a file to upload:</label> 
            <input name="uploadedfile" type="file" id="uploadfile_input" />
        </p>
        <p>
            <input type="submit" value="Upload File" />
        </p>
    </form>

    <form enctype="multipart/form-data" action="delete.php" method="POST">
        <p>
            <input type="hidden" name="MAX_FILE_SIZE" value="20000000" />
            <label for="deletefile_input">Choose a file to delete:</label> 
            <input name="deletedfile" type="file" id="deletefile_input" />
        </p>
        <p>
            <input type="submit" value="Delete File" />
        </p>
    </form>
    </body>
</html>