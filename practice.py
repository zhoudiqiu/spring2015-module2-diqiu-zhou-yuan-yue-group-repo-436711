import os.path
import tornado.locale
import tornado.httpserver
import tornado.ioloop
import tornado.options
import tornado.web
import tornado.httpclient
import tornado.gen
from tornado.options import define, options
import concurrent.futures
from concurrent.futures import ThreadPoolExecutor
import json
import boto
from tornado.escape import json_encode
from boto.s3.connection import S3Connection
from boto.s3.key import Key
import urllib

define("port", default=8000, help="run on the given port", type=int)

class Application(tornado.web.Application):
	def __init__(self):
		self.handlers=[
			(r"/", MainHandler),
			(r"/post",PostHandler),
		]
	settings = {
		"debug":True,
		}

#1st tornado handler to say hello world	
class MainHandler(tornado.web.RequestHandler):
	def get(self):
		self.write("Hello, world!")

#Hander to push a file to my AWS S3 Service bucket
class PostHandler(tornado.web.RequestHandler):
		@tornado.gen.coroutine
		def send_file(self,k):
			return k.set_contents_from_filename('temp.json')
		
		@tornado.gen.coroutine
		def post(self):
			f = open('temp.json', 'w')
			f.write(self.request.body.decode('utf-8'))
			f.close()
			#This key pair is enabled
			c = S3Connection('AKIAJS5G75H3SGUNKUFQ', 'UfUVRCPtWr+hbdvUmEBP4xAC8FVW3eFM4hrD8B1E')
			b = c.get_bucket('pongoboy')
			k = Key(b)
			k.key = 'f'
			result = yield self.send_file(k)
			if result:
				self.write({"result":"ok"})
			

			

if __name__ == "__main__":
    tornado.options.parse_command_line()
    app = tornado.web.Application(
    	[(r"/", MainHandler),
			(r"/post",PostHandler),]
			,debug=True
    	)
    http_server = tornado.httpserver.HTTPServer(app)
    http_server.listen(8080)
    tornado.ioloop.IOLoop.instance().start()