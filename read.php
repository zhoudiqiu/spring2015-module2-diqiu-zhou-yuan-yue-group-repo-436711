
<?php
	session_start();
 
	$filename = $_POST['file_name'];
	 
	if( !preg_match('/^[\w_\.\-]+$/', $filename) ){
		echo "Invalid filename";
		exit;
	}
 
	$username = $_SESSION['username'];
	if( !preg_match('/^[\w_\-]+$/', $username) ){
		echo "Invalid username";
		exit;
	}
 
	$pathToFile = sprintf("/srv/uploads/%s/%s", $username, $filename);

	if (file_exists($pathToFile)) {
		$finfo = new finfo(FILEINFO_MIME_TYPE);
		$contentType = $finfo->file($pathToFile);
		header("Content-Type: ".$contentType);
		readfile($pathToFile);
	}else {
		echo "No files in directory";
	}
?>
